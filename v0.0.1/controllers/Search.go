package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"kumparan-article-search/constants"
	"kumparan-article-search/models"
	"kumparan-article-search/utils"
	"kumparan-article-search/v0.0.1/services"
	"net/http"
	lg "ottodigital.id/library/logger/v2"
)

// Search data Article
// Article - Search godoc
// @Summary Article - Search
// @Description Article - Search
// @ID Article - Search
// @Tags Article
// @Router /kumparan/v0.0.1/articles [get]
// @Accept json
// @Produce json
// @Success 200 {object} models.Response{} "Article - Search EXAMPLE"
func Search(ctx *gin.Context) {
	log := lg.InitLogs(ctx.Request)

	res := models.Response{
		Meta: utils.GetMetaResponse(constants.KeyResponseDefault),
	}

	query := ctx.Query("query")
	author := ctx.Query("author")

	log.Info("Article-Search  Controller",
		log.AddField("RequestParams:", string(query+author)))

	services.InitiateService(log).Search(query, author, &res)

	resBytes, _ := json.Marshal(res)
	log.Info("Article-Search Controller",
		log.AddField("ResponseBody: ", string(resBytes)))

	ctx.JSON(http.StatusOK, res)
}
