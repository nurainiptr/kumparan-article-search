package controllers

import (
	"github.com/stretchr/testify/assert"
	"kumparan-article-search/models"
	"kumparan-article-search/v0.0.1/services"
	lg "ottodigital.id/library/logger/v2"
	"testing"
)

func Test_Search(t *testing.T) {
	var lg lg.OttologInterface
	var res *models.Response

	query := "Covid"
	author := "putri"
	services.InitiateService(lg).Search(query, author, res)
	assert.Equal(t, 200, res.Meta.Code, "OK response is expected")
}
