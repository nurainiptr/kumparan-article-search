package services

import (
	"kumparan-article-search/models"
	ottologger "ottodigital.id/library/logger/v2"
)

// Service ..
type Service struct {
	General models.GeneralModel
	OttoLog ottologger.OttologInterface
}

// ServiceInterface ..
type ServiceInterface interface {
	Search(string, string, *models.Response)
}

// InitiateService ..
func InitiateService(log ottologger.OttologInterface) ServiceInterface {
	return &Service{
		OttoLog: log,
	}
}
