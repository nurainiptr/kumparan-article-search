package services

import (
	"kumparan-article-search/constants"
	"kumparan-article-search/database/mongodb"
	"kumparan-article-search/models"
	"kumparan-article-search/utils"
)

// Search to searching article
func (svc *Service) Search(query string, author string, res *models.Response) {
	dataDB, errDB := mongodb.Find(query, author)
	if errDB != nil {
		res.Meta = utils.GetMetaResponse(constants.KeyResponseDefault)
		return
	}

	res.Data = dataDB
	res.Meta = utils.GetMetaResponse(constants.KeyResponseSuccessful)
	return
}
