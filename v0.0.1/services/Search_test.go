package services

import (
	"kumparan-article-search/models"
	"reflect"
	"testing"
)

func TestService_Search(t *testing.T) {
	type fields struct {
		General models.GeneralModel
	}
	type args struct {
		query    string
		author   string
		response *models.Response
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.Response
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			service := &Service{
				General: tt.fields.General,
			}
			if service.Search(tt.args.query, tt.args.author, tt.args.response); !reflect.DeepEqual(tt.args.response, tt.want) {
				t.Errorf("service.Search = %v, want %v", tt.args.response, tt.want)
			}
		})
	}
}
