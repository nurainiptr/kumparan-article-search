package routers

import (
	"fmt"
	"github.com/astaxie/beego/logs"
	"io"
	"kumparan-article-search/docs"
	v001Controller "kumparan-article-search/v0.0.1/controllers"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/kelseyhightower/envconfig"
	"github.com/opentracing/opentracing-go"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/uber/jaeger-client-go"
	"ottodigital.id/library/httpserver/ginserver"
	"ottodigital.id/library/ottotracing"
	"ottodigital.id/library/utils"
)

// ServerEnv ..
type ServerEnv struct {
	ServiceName     string `envconfig:"SERVICE_NAME" default:"KUMPARAN-ARTICLE-SEARCH"`
	OpenTracingHost string `envconfig:"OPEN_TRACING_HOST" default:"13.250.21.165:5775"`
	DebugMode       string `envconfig:"DEBUG_MODE" default:"debug"`
	ReadTimeout     int    `envconfig:"READ_TIMEOUT" default:"120"`
	WriteTimeout    int    `envconfig:"WRITE_TIMEOUT" default:"120"`
}

var (
	server ServerEnv
)

func init() {
	if err := envconfig.Process("SERVER", &server); err != nil {
		fmt.Println("Failed to get SERVER env:", err)
	}
}

// Server ..
func Server(listenAddress string) error {
	oRouter := ORouter{}
	oRouter.InitTracing()
	oRouter.Routers()
	defer oRouter.Close()

	err := ginserver.GinServerUp(listenAddress, oRouter.Router)
	if err != nil {
		logs.Error(fmt.Sprintf("Error: ", err))
		return err
	}

	fmt.Println("Server UP")
	return err
}

// ORouter ..
type ORouter struct {
	Tracer   opentracing.Tracer
	Reporter jaeger.Reporter
	Closer   io.Closer
	Err      error
	GinFunc  gin.HandlerFunc
	Router   *gin.Engine
}

// Routers ..
func (oRouter *ORouter) Routers() {
	gin.SetMode(server.DebugMode)

	// programmatically set swagger info
	docs.SwaggerInfo.Title = "kumparan-article-search API"
	docs.SwaggerInfo.Description = "<kumparan-article-search description>"
	docs.SwaggerInfo.Version = "1.0"
	//docs.SwaggerInfo.BasePath = "/ottosfa-api-apk"
	docs.SwaggerInfo.Schemes = []string{"http"}
	switch utils.GetEnv("APPS_ENV", "local") {
	case "local":
		docs.SwaggerInfo.Host = utils.GetEnv("SWAGGER_HOST_LOCAL", "localhost:8145")
	case "dev":
		docs.SwaggerInfo.Host = utils.GetEnv("SWAGGER_HOST_DEV", "13.228.26.86:8145")
	}

	router := gin.New()
	router.Use(oRouter.GinFunc)
	router.Use(gin.Recovery())

	router.Use(cors.New(cors.Config{
		AllowMethods:     []string{"GET", "POST", "OPTIONS", "DELETE", "PUT"},
		AllowHeaders:     []string{"Origin", "authorization", "Content-Length", "Content-Type", "User-Agent", "Referrer", "Host", "Token"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		AllowCredentials: true,
		AllowAllOrigins:  true,
		//AllowOriginFunc:  func(origin string) bool { return true },
		MaxAge: 86400,
	}))

	v1 := router.Group("/kumparan/v0.0.1")
	{
		v1.GET("/articles", v001Controller.Search)
	}

	// use ginSwagger middleware to serve the API docs
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	oRouter.Router = router

}

// InitTracing ..
func (oRouter *ORouter) InitTracing() {
	hostName, err := os.Hostname()
	if err != nil {
		hostName = "PROD"
	}

	tracer, reporter, closer, err := ottotracing.InitTracing(fmt.Sprintf("%s::%s", server.ServiceName, hostName), server.OpenTracingHost, ottotracing.WithEnableInfoLog(true))
	if err != nil {
		fmt.Println("Error :", err)
	}
	opentracing.SetGlobalTracer(tracer)

	oRouter.Closer = closer
	oRouter.Reporter = reporter
	oRouter.Tracer = tracer
	oRouter.Err = err
	oRouter.GinFunc = ottotracing.OpenTracer([]byte("api-request-"))
}

// Close ..
func (oRouter *ORouter) Close() {
	oRouter.Closer.Close()
	oRouter.Reporter.Close()
}
