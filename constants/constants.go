package constants

const (
	// KeyResponseDefault for key default response
	KeyResponseDefault = "default"

	// KeyResponseSuccessful for key success response
	KeyResponseSuccessful = "successful"
)
