package mongodb

import (
	"fmt"
	"github.com/astaxie/beego/logs"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

// Articles ..
type Articles struct {
	Id      int       `bson:"id"`
	Author  string    `bson:"author"`
	Title   string    `bson:"title"`
	Body    string    `bson:"body"`
	Created time.Time `bson:"created"`
}

// Insert ..
func Insert(req Articles) error {
	fmt.Println(">>> Article - Insert - Postgres <<<")
	_, err := Dbcon.Collection("articles").InsertOne(ctx, req)
	if err != nil {
		logs.Error(fmt.Sprintf("Failed connect to database MongoDB when insert article", err))
		return err
	}
	return nil
}

//Find ..
func Find(query, author string) ([]Articles, error) {
	filter := bson.D{
		{"author", primitive.Regex{Pattern: author, Options: ""}},
		{"title", primitive.Regex{Pattern: query, Options: ""}},
		{"body", primitive.Regex{Pattern: query, Options: ""}}}

	findOptions := options.Find()
	findOptions.SetSort(bson.D{{"created", -1}})

	csr, err := Dbcon.Collection("articles").Find(ctx, filter, findOptions)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer csr.Close(ctx)

	result := make([]Articles, 0)
	for csr.Next(ctx) {
		var row Articles
		err := csr.Decode(&row)
		if err != nil {
			log.Fatal(err.Error())
		}
		result = append(result, row)
	}
	return result, err
}
