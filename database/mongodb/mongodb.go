package mongodb

import (
	"context"
	"fmt"
	"github.com/astaxie/beego/logs"
	"github.com/kelseyhightower/envconfig"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// DbEnv ..
type DbEnv struct {
	DbApplyURI string `envconfig:"DB_MONGODB_APPLYURI" default:"mongodb://127.0.0.1:27018"`
	DbName     string `envconfig:"DB_MONGODB_NAME" default:"article-mongodb"`
}

var (
	// Dbcon ..
	Dbcon *mongo.Database
	dbEnv DbEnv
	ctx   = context.Background()
)

// init ..
func init() {
	err := envconfig.Process("Database_MongoDB", &dbEnv)
	if err != nil {
		fmt.Println("Failed to get Database MongoDB env:", err)
	}

	Dbcon, err = GetDbCon()
	if err != nil {
		logs.Error(fmt.Sprintf("Db Not Connect:", err))
	}
}

// GetDbCon ..
func GetDbCon() (*mongo.Database, error) {
	clientOptions := options.Client()
	clientOptions.ApplyURI(dbEnv.DbApplyURI)
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		return nil, err
	}

	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}

	return client.Database(dbEnv.DbName), nil
}
