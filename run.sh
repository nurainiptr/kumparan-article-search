export LOGGER_FILENAME="./logger.log"

#main.go
export KUMPARAN_ARTICLE_SEARCH_PORT=0.0.0.0:8145
export MAXPROCS="1"

#routes/route.go
export KUMPARAN_ARTICLE_SEARCH="KUMPARAN_ARTICLE_SEARCH"
export READ_TIMEOUT="120"
export WRITE_TIMEOUT="120"

#db/mongodb.go
export DB_MONGODB_APPLYURI="mongodb://127.0.0.1:27018"
export DB_MONGODB_NAME="article-mongodb"

#kafka
export KAFKA_GROUP="kumparan"
export KAFKA_BROKERS="17.229.100.66:7097,17.229.100.66:7098,17.229.100.66:7099"
export KAFKA_ZOOKEEPER="23.229.100.66:2182,23.229.100.66:2183,23.229.100.66:2184"
export DEV_KAFKA_TOPICS="DEV-article"
export STAG_KAFKA_TOPICS="STAG-article"
export PROD_KAFKA_TOPICS="PROD-article"
export TYPE="DEV"

#router/routers.go
#swagger
export APPS_ENV="local"
export SWAGGER_HOST_LOCAL="localhost:8145"
export SWAGGER_HOST_DEV="13.228.26.86:8145"

go run main.go
