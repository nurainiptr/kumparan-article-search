package services

import (
	"github.com/Shopify/sarama"
	"github.com/wvanbergen/kafka/consumergroup"
	"time"
)

// ConfigKafka ..
type ConfigKafka struct {
	ConsumerGroup  string
	KafkaTopics    []string
	Zookeeper      []string
	ConsumerConfig *consumergroup.Config
}

// GetConsumerConfig ..
func GetConsumerConfig() *consumergroup.Config {
	config := consumergroup.NewConfig()
	config.Offsets.Initial = sarama.OffsetNewest
	config.Offsets.ProcessingTimeout = 10 * time.Second
	return config
}
