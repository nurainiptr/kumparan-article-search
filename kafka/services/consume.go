package services

import (
	"encoding/json"
	"fmt"
	"kumparan-article-search/database/mongodb"
	kafka "kumparan-article-search/kafka"
	"kumparan-article-search/models"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/logs"
	cluster "github.com/bsm/sarama-cluster"
	"ottodigital.id/library/utils"
)

var (
	typeWorker          string
	consumerGroup       string
	kafkaTopics         string
	kafkaTopicsDEV      string
	kafkaTopicsSTAG     string
	kafkaTopicsPROD     string
	kafkaZookeeper      string
	kafkaBrokerUrl      string
	kafkaTopicsSFAError string
	offsetkafka         int
)

func init() {
	consumerGroup = utils.GetEnv("KAFKA_GROUP", "kumparan")
	kafkaBrokerUrl = utils.GetEnv("KAFKA_BROKERS", "17.229.100.66:7097,17.229.100.66:7098,17.229.100.66:7099")
	kafkaZookeeper = utils.GetEnv("KAFKA_ZOOKEEPER", "23.229.100.66:2182,23.229.100.66:2183,23.229.100.66:2184")
	offsetkafka, _ = strconv.Atoi(utils.GetEnv("KAFKA_OFFSET", "1"))
	typeWorker = utils.GetEnv("TYPE", "DEV")
	kafkaTopicsDEV = utils.GetEnv("DEV_KAFKA_TOPICS", "DEV-article")
	kafkaTopicsSTAG = utils.GetEnv("STAG_KAFKA_TOPICS", "STAG-article")
	kafkaTopicsPROD = utils.GetEnv("PRODKAFKA_TOPICS", "PROD-article")
}

// StartServicesKafka ..
func StartServicesKafka() {
	fmt.Println("Start Services Kafka")
	configKafka := kafka.ConfigKafka{}
	configKafka.ConsumerGroup = consumerGroup

	switch typeWorker {
	case "DEV":
		kafkaTopics = kafkaTopicsDEV
		break
	case "STAG":
		kafkaTopics = kafkaTopicsSTAG
		break
	case "PROD":
		kafkaTopics = kafkaTopicsPROD
		break
	}

	configKafka.KafkaTopics = strings.Split(kafkaTopics, ",")
	configKafka.Zookeeper = strings.Split(kafkaZookeeper, ",")
	configKafka.ConsumerConfig = kafka.GetConsumerConfig()

	config := cluster.NewConfig()
	config.Consumer.Return.Errors = true
	config.Group.Return.Notifications = true
	config.Consumer.Offsets.CommitInterval = time.Duration(1)
	if offsetkafka == 0 {
		config.Consumer.Offsets.Initial = -2
	}

	listBroker := strings.Split(kafkaBrokerUrl, ",")
	topics := strings.Split(kafkaTopics, ",")
	consumer, err := cluster.NewConsumer(listBroker, consumerGroup, topics, config)

	if err != nil {
		logs.Error(fmt.Sprintf("Cluster Connect problem: ", err))
	}

	go func() {
		for err := range consumer.Errors() {
			logs.Error(fmt.Sprintf("Consumer Error: ", err))
		}
	}()

	go func() {
		for ntf := range consumer.Notifications() {
			logs.Info("Rebalanced: %+v\n", ntf)
		}
	}()

	for {
		select {
		case msg, ok := <-consumer.Messages():
			if ok {
				go Processing(msg.Value)
				consumer.MarkOffset(msg, "")
			}
		}
	}
}

// Processing ..
func Processing(data []byte) {
	article := models.Articles{}
	json.Unmarshal(data, &article)

	ArticleReq := mongodb.Articles{
		Id:      article.Id,
		Author:  article.Author,
		Title:   article.Title,
		Body:    article.Body,
		Created: article.Created,
	}

	errDB := mongodb.Insert(ArticleReq)
	if errDB != nil {
		return
	}
}
